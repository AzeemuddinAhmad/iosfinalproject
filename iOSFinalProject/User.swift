//
//  User.swift
//  iOSFinalProject
//
//  Created by Azeemuddin Ahmad on 12/9/1398 AP.
//  Copyright © 1398 Azeemuddin Ahmad. All rights reserved.
//

import Foundation

class user {
    let username: String, email: String, contactNumber: String, password: String
    
    init(username: String, email: String, contactNumber: String, password: String) {
        self.username = username
        self.email = email
        self.contactNumber = contactNumber
        self.password = password
    }
}
