import UIKit

class UserCategoryItemVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var pageControl: UIPageControl!
    
    var category: Category!
    var itemImageName = ""
    
    enum Identifier {
        static let reuseIdentifier = "UserCategoryItem"
        static let cellNameIdentifier = "UserCategoryItemCell"
        static let backgroundImageIdentifier = "CategoryBackground.jpg"
        static let segueIdentifier = "ViewInAR"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: Identifier.backgroundImageIdentifier)!)
        let cellNib = UINib(nibName: Identifier.cellNameIdentifier, bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: Identifier.reuseIdentifier)
        pageControl.numberOfPages = collectionView.numberOfItems(inSection: 0)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Identifier.segueIdentifier {
            let controller = segue.destination as! ViewInAugmentedRealityVC
            controller.imageName = itemImageName
        }
    }
    
    func configureCell(cell: UserCategoryItemCell, index: Int) {
        let categoryItem = category.items[index]
        cell.delegate = self
        cell.itemImageView.image = UIImage(named: categoryItem.imageName)
        itemImageName = categoryItem.imageName
        cell.nameTextField.text = categoryItem.itemName
        cell.descriptionTextView.text = categoryItem.itemDescription
        cell.priceTextField.text = String(categoryItem.itemPrice)
    }
}

extension UserCategoryItemVC {
    @IBAction fileprivate func prevButton(_ sender: Any) {
        let prevIndex = max(0, pageControl.currentPage - 1)
        let indexPath = IndexPath(item: prevIndex, section: 0)
        pageControl.currentPage = prevIndex
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    @IBAction fileprivate func nextButton(_ sender: Any) {
        let nextIndex = min(pageControl.currentPage + 1, collectionView.numberOfItems(inSection: 0) - 1 )
        let indexPath = IndexPath(item: nextIndex, section: 0)
        pageControl.currentPage = nextIndex
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}

extension UserCategoryItemVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category.items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifier.reuseIdentifier, for: indexPath) as! UserCategoryItemCell
        configureCell(cell: cell, index: indexPath.item)
        return cell
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        pageControl.currentPage = Int (x / view.safeAreaLayoutGuide.layoutFrame.width)
    }
    
}

extension UserCategoryItemVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.safeAreaLayoutGuide.layoutFrame.width, height: view.safeAreaLayoutGuide.layoutFrame.height - 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension UserCategoryItemVC: UserCategoryItemCellDelegate {
    func viewAsAR() {
        performSegue(withIdentifier: Identifier.segueIdentifier, sender: nil)
    }
}
