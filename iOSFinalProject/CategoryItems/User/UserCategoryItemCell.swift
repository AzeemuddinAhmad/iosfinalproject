//
//  UserCategoryItemCell.swift
//  iOSFinalProject
//
//  Created by Azeemuddin Ahmad on 12/17/1398 AP.
//  Copyright © 1398 Azeemuddin Ahmad. All rights reserved.
//

import UIKit

protocol  UserCategoryItemCellDelegate: class {
    func viewAsAR()
}

class UserCategoryItemCell: UICollectionViewCell {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var viewInARButton: UIButton!
    
    var delegate: UserCategoryItemCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension UserCategoryItemCell {
    @IBAction fileprivate func viewInAR (_ sender: Any) {
        delegate?.viewAsAR()
    }
}
