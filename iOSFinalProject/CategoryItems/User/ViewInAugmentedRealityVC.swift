import UIKit
import SceneKit
import ARKit

class ViewInAugmentedRealityVC: UIViewController {

    @IBOutlet weak var sceneView: ARSCNView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var upButton: UIButton!
    @IBOutlet weak var downButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var topRightButton: UIButton!
    @IBOutlet weak var bottomLeftButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var rotateButton: UIButton!
    @IBOutlet weak var locateButton: UIButton!
    
    var rotate: Bool = false
    var locate: Bool = false
    var imageName = ""
    var scene = SCNScene()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSession()
        initSceneView()
        initARSession()
        addTapGestureToSceneView()
        print(imageName)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        hideSettingButtons()
        rotateButton.isHidden = true
        locateButton.isHidden = true
    }
    
    // MARK: - Initialization
    
    func initSession() {
        
    }
    
    func initSceneView() {
        sceneView.delegate = self
        sceneView.showsStatistics = true
        //sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        statusLabel.text = "Greetings! :)"
        
    }
    
    func initARSession() {
        guard ARWorldTrackingConfiguration.isSupported else {
            print("*** ARConfig: AR World Tracking Not Supported")
            return
        }
        
        let config = ARWorldTrackingConfiguration()
        config.worldAlignment = .gravity
        config.providesAudioData = false
        config.planeDetection = [.horizontal , .vertical]
        sceneView.session.run(config)
    }
    
    func addTapGestureToSceneView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewInAugmentedRealityVC.addShipToSceneView(withGestureRecognizer:)))
        sceneView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func hideSettingButtons() {
        upButton.isHidden = true
        downButton.isHidden = true
        leftButton.isHidden = true
        rightButton.isHidden = true
        topRightButton.isHidden = true
        bottomLeftButton.isHidden = true
        doneButton.isHidden = true
    }
    
    func showSettingButtons() {
        upButton.isHidden = false
        downButton.isHidden = false
        leftButton.isHidden = false
        rightButton.isHidden = false
        topRightButton.isHidden = false
        bottomLeftButton.isHidden = false
        doneButton.isHidden = false
    }
}

extension ViewInAugmentedRealityVC {
    @objc func addShipToSceneView(withGestureRecognizer recognizer: UIGestureRecognizer) {
        let tapLocation = recognizer.location(in: sceneView)
        let hitTestResults = sceneView.hitTest(tapLocation, types: .existingPlaneUsingExtent)
        
        guard let hitTestResult = hitTestResults.first else { return }
        let translation = hitTestResult.worldTransform.translation
        let x = translation.x
        let y = translation.y
        let z = translation.z
        
        guard let shipScene = SCNScene(named: "iOSFinalProject.scnassets/\(imageName).dae"),
            let shipNode = shipScene.rootNode.childNode(withName: imageName, recursively: false)
            else { return }
        
        shipNode.position = SCNVector3(x,y,z)
        shipNode.scale = SCNVector3(0.05, 0.05, 0.05)
        sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.removeFromParentNode()
        sceneView.scene.rootNode.addChildNode(shipNode)
        locateButton.isHidden = false
        rotateButton.isHidden = false
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) in
            guard let planeNode = node.childNode(withName: "Plane", recursively: false),
            let plane = planeNode.geometry as? SCNPlane else { return }
            plane.materials.first?.diffuse.contents = UIColor.clear
        }
    }
    
    @IBAction fileprivate func rotate(_ sender: Any) {
        locate = false
        rotate = true
        statusLabel.text = "Rotation enabled"
        showSettingButtons()
    }
    
    @IBAction fileprivate func locate(_ sender: Any) {
        rotate = false
        locate = true
        statusLabel.text = "Location enabled"
        showSettingButtons()
    }
    
    @IBAction fileprivate func done(_ sender: Any) {
        rotate = false
        locate = false
        statusLabel.text = "All Done?"
        hideSettingButtons()
    }
    
    @IBAction fileprivate func up(_ sender: Any) {
        if rotate == true {
            let rotateAction = SCNAction.rotateBy(x: -0.5, y: 0, z: 0, duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(rotateAction)
        } else if locate == true {
            let moveAction = SCNAction.move(by: SCNVector3(0, 0.05, 0), duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(moveAction)
        }
    }
    
    @IBAction fileprivate func down(_ sender: Any) {
        if rotate == true {
            let rotateAction = SCNAction.rotateBy(x: 0.5, y: 0, z: 0, duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(rotateAction)
        } else if locate == true {
            let moveAction = SCNAction.move(by: SCNVector3(0, -0.05, 0), duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(moveAction)
        }
    }
    
    @IBAction fileprivate func left(_ sender: Any) {
        if rotate == true {
            let rotateAction = SCNAction.rotateBy(x: 0, y: 0.5, z: 0, duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(rotateAction)
        } else if locate == true {
            let moveAction = SCNAction.move(by: SCNVector3(-0.05, 0, 0), duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(moveAction)
        }
    }
    
    @IBAction fileprivate func right(_ sender: Any) {
        if rotate == true {
            let rotateAction = SCNAction.rotateBy(x: 0, y: -0.5, z: 0, duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(rotateAction)
        } else if locate == true {
            let moveAction = SCNAction.move(by: SCNVector3(0.05, 0, 0), duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(moveAction)
        }
    }
    
    @IBAction fileprivate func topRight(_ sender: Any) {
        if rotate == true {
            let rotateAction = SCNAction.rotateBy(x: 0, y: 0, z: -0.5, duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(rotateAction)
        } else if locate == true {
            let moveAction = SCNAction.move(by: SCNVector3(0, 0, 0.05), duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(moveAction)
        }
    }
    
    @IBAction fileprivate func bottomLeft(_ sender: Any) {
        if rotate == true {
            let rotateAction = SCNAction.rotateBy(x: 0, y: 0, z: 0.5, duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(rotateAction)
        } else if locate == true {
            let moveAction = SCNAction.move(by: SCNVector3(0, 0, -0.05), duration: 0.25)
            sceneView.scene.rootNode.childNode(withName: imageName, recursively: false)?.runAction(moveAction)
        }
    }
}

extension ViewInAugmentedRealityVC: ARSCNViewDelegate {
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as? ARPlaneAnchor else { return }

        let width = CGFloat(planeAnchor.extent.x)
        let height = CGFloat(planeAnchor.extent.z)
        let plane = SCNPlane(width: width, height: height)
        plane.materials.first?.diffuse.contents = UIColor.systemBlue

        let planeNode = SCNNode(geometry: plane)

        let x = CGFloat(planeAnchor.center.x)
        let y = CGFloat(planeAnchor.center.y)
        let z = CGFloat(planeAnchor.center.z)
        planeNode.position = SCNVector3(x,y,z)
        planeNode.opacity = 0.0
        planeNode.name = "Plane"
        planeNode.eulerAngles.x = -.pi / 2
        DispatchQueue.main.async {
            self.statusLabel.text = "Surface Detected"
        }
        
        node.addChildNode(planeNode)
        
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
        guard let planeAnchor = anchor as?  ARPlaneAnchor,
            let planeNode = node.childNodes.first,
            let plane = planeNode.geometry as? SCNPlane
            else { return }

        let width = CGFloat(planeAnchor.extent.x)
        let height = CGFloat(planeAnchor.extent.z)
        plane.width = width
        plane.height = height

        let x = CGFloat(planeAnchor.center.x)
        let y = CGFloat(planeAnchor.center.y)
        let z = CGFloat(planeAnchor.center.z)
        planeNode.position = SCNVector3(x, y, z)
    }

}

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}
