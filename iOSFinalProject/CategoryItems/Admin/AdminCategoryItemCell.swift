//
//  AdminCategoryItemCell.swift
//  iOSFinalProject
//
//  Created by Azeemuddin Ahmad on 12/12/1398 AP.
//  Copyright © 1398 Azeemuddin Ahmad. All rights reserved.
//

import UIKit

protocol AdminCategoryItemCellDelegate: class {
    func didEdit(indexPath: IndexPath)
    func didDelete(indexPath: IndexPath)
}

class AdminCategoryItemCell: UICollectionViewCell {
    
    @IBOutlet weak var itemPriceTextField: UITextField!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemNameTextField: UITextField!
    @IBOutlet weak var itemDescriptionTextView: UITextView!

    weak var delegate: AdminCategoryItemCellDelegate?
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}

extension AdminCategoryItemCell {
    @IBAction fileprivate func didEdit(_ sender: Any) {
        delegate?.didEdit(indexPath: indexPath!)
    }
    
    @IBAction fileprivate func didDelete(_ sender: Any) {
        delegate?.didDelete(indexPath: indexPath!)
    }
}
