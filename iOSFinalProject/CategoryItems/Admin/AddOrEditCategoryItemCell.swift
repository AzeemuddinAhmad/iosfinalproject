import UIKit

protocol AddOrEditCategoryItemCellDelegate: class {
    func imagePickerVC()
}

class AddOrEditCategoryItemCell: UICollectionViewCell {
    
    @IBOutlet weak var nameView: UIView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var descriptionView: UIView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var itemImageView: UIImageView!
    
    var delegate: AddOrEditCategoryItemCellDelegate?

    override func awakeFromNib() {
        super.awakeFromNib()
        nameView.layer.cornerRadius = 10
        priceView.layer.cornerRadius = 10
        descriptionView.layer.cornerRadius = 10
    }

}

extension AddOrEditCategoryItemCell {
    @IBAction fileprivate func selectImage(_ sender: Any) {
        delegate?.imagePickerVC()
    }
}
