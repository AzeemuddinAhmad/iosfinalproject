import UIKit

protocol CategoryItemImagePickerVCDelegate: class {
    func categoryItemImagePicker(_ picker: CategoryItemImagePickerVC, didPick imageName: String)
}

class CategoryItemImagePickerVC: UITableViewController {
    
    weak var delegate: CategoryItemImagePickerVCDelegate?
    let images = ["No Icon", "RusticChair", "TrescaniSofa", "BrownSofa", "BlueSofa"]

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return images.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "IconCell", for: indexPath)
        let iconName = images[indexPath.row]
        cell.textLabel!.text = iconName
        cell.imageView?.image = UIImage(named: iconName)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let delegate = delegate {
            let imageName = images[indexPath.row]
            delegate.categoryItemImagePicker(self, didPick: imageName)
        }
    }

}
