import UIKit

protocol AddOrEditCategoryItemVCDelegate: class {
    func addOrEditCategoryItemVCDidCancel(_ controller: AddOrEditCategoryItemVC)
    func addOrEditCategoryItemVC(_ controller: AddOrEditCategoryItemVC, didFinishAdding cateogryItem: CategoryItems)
    func addOrEditCategoryItemVC(_ controller: AddOrEditCategoryItemVC, didFinishEditing cateogryItem: CategoryItems)
}

class AddOrEditCategoryItemVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var cell = AddOrEditCategoryItemCell()
    var itemToEdit: CategoryItems?
    var itemImageName = ""
    weak var delegate: AddOrEditCategoryItemVCDelegate?
    
    enum Identifier {
        static let reuseIdentifier = "AddOrEditCell"
        static let cellNameIdentifier = "AddOrEditCategoryItemCell"
        static let backgroundImageIdentifier = "CategoryBackground"
        static let plistIdentifier = "Categories.plist"
        static let imagePickerSegue = "ImagePicker"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundView = UIImageView(image: UIImage(named: Identifier.backgroundImageIdentifier))
        let cellNib = UINib(nibName: Identifier.cellNameIdentifier, bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: Identifier.reuseIdentifier)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Identifier.imagePickerSegue {
            let controller = segue.destination as! CategoryItemImagePickerVC
            controller.delegate = self
        }
    }

}

extension AddOrEditCategoryItemVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifier.reuseIdentifier, for: indexPath) as! AddOrEditCategoryItemCell
        if let categoryItem = itemToEdit {
            title = "Edit Category Item"
            cell.nameTextField.text = categoryItem.itemName
            cell.descriptionTextView.text = categoryItem.itemDescription
            cell.priceTextField.text = String(categoryItem.itemPrice)
            cell.itemImageView.image = UIImage(named: categoryItem.imageName)
        }
        cell.delegate = self
        return cell
    }
    
    
}

extension AddOrEditCategoryItemVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.safeAreaLayoutGuide.layoutFrame.width, height: view.safeAreaLayoutGuide.layoutFrame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}

extension AddOrEditCategoryItemVC: AddOrEditCategoryItemCellDelegate {
    func imagePickerVC() {
        performSegue(withIdentifier: Identifier.imagePickerSegue, sender: nil)
    }
    
    
}

extension AddOrEditCategoryItemVC: CategoryItemImagePickerVCDelegate {
    func categoryItemImagePicker(_ picker: CategoryItemImagePickerVC, didPick imageName: String) {
        let cell = collectionView.cellForItem(at: IndexPath(item: 0, section: 0 )) as! AddOrEditCategoryItemCell
        cell.itemImageView.image = UIImage(named: imageName)
        itemImageName = imageName
        navigationController?.popViewController(animated: true)
    }
    
    
}

extension AddOrEditCategoryItemVC {
    @IBAction fileprivate func done(_ sender: Any) {
        if let collectionView = self.collectionView {
            let indexPath = collectionView.indexPathsForVisibleItems
            let cell = collectionView.cellForItem(at: indexPath[0] ) as! AddOrEditCategoryItemCell
            if let categoryItem = itemToEdit {
                categoryItem.itemName = cell.nameTextField.text!
                categoryItem.itemDescription = cell.descriptionTextView.text!
                categoryItem.itemPrice = Float(cell.priceTextField.text!)!
                categoryItem.imageName = itemImageName
                delegate?.addOrEditCategoryItemVC(self, didFinishEditing: categoryItem)
            } else {
                let categoryItem = CategoryItems.init(itemName: cell.nameTextField.text!, itemDescription: cell.descriptionTextView.text!,imageName: itemImageName, itemPrice: Float(cell.priceTextField.text!)!)
                delegate?.addOrEditCategoryItemVC(self, didFinishAdding: categoryItem)
            }
        }
        
    }
    
    @IBAction fileprivate func cancel(_ sender: Any) {
        print("Cancel")
        delegate?.addOrEditCategoryItemVCDidCancel(self)
    }
}
