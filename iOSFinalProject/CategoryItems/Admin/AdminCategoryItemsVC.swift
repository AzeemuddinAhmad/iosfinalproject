import UIKit

class AdminCategoryItemsVC: UIViewController {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!

    var category: Category!
    
    enum Identifier {
        static let reuseIdentifier = "AdminCategoryItem"
        static let cellNameIdentifier = "AdminCategoryItemCell"
        static let backgroundImageIdentifier = "CategoryBackground.jpg"
        static let plistIdentifier = "Categories.plist"
        static let addCategoryItemSegue = "AddCategoryItem"
        static let navigationControllerIdentifier = "AddOrEditCategoryItemNC"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: Identifier.backgroundImageIdentifier)!)
        title = category.name
        collectionView.delegate = self
        collectionView.dataSource = self
        let cellNib = UINib(nibName: Identifier.cellNameIdentifier, bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: Identifier.reuseIdentifier)
        pageControl.numberOfPages = collectionView.numberOfItems(inSection: 0)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Identifier.addCategoryItemSegue {
            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! AddOrEditCategoryItemVC
            controller.delegate = self
        }
    }
   
    func configureCell(cell: AdminCategoryItemCell, index: Int) {
        let categoryItem = category.items[index]
        cell.delegate = self
        cell.indexPath = IndexPath(item: index, section: 0)
        cell.itemImageView.image = UIImage(named: categoryItem.imageName)
        cell.itemNameTextField.text = categoryItem.itemName
        cell.itemPriceTextField.text = String(categoryItem.itemPrice)
        cell.itemDescriptionTextView.text = categoryItem.itemDescription
    }
}

extension AdminCategoryItemsVC {
    @IBAction func prevButton(_ sender: Any) {
        let prevIndex = max(0, pageControl.currentPage - 1)
        let indexPath = IndexPath(item: prevIndex, section: 0)
        pageControl.currentPage = prevIndex
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func nextButton(_ sender: Any) {
        let nextIndex = min(pageControl.currentPage + 1, collectionView.numberOfItems(inSection: 0) - 1 )
        let indexPath = IndexPath(item: nextIndex, section: 0)
        pageControl.currentPage = nextIndex
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}

extension AdminCategoryItemsVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return category.items.count
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        pageControl.currentPage = Int (x / view.safeAreaLayoutGuide.layoutFrame.width)
    }
}

extension AdminCategoryItemsVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifier.reuseIdentifier, for: indexPath) as! AdminCategoryItemCell
        configureCell(cell: cell, index: indexPath.item)
        return cell
    }
}

extension AdminCategoryItemsVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.safeAreaLayoutGuide.layoutFrame.width, height: view.safeAreaLayoutGuide.layoutFrame.height - 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
}

extension AdminCategoryItemsVC: AddOrEditCategoryItemVCDelegate {
    func addOrEditCategoryItemVCDidCancel(_ controller: AddOrEditCategoryItemVC) {
        dismiss(animated: true, completion: nil)
    }
    
    func addOrEditCategoryItemVC(_ controller: AddOrEditCategoryItemVC, didFinishAdding cateogryItem: CategoryItems) {
        let newItemIndex = category.items.count
        category.items.append(cateogryItem)
        
        let indexPath = IndexPath(item: newItemIndex, section: 0)
        let indexPaths = [indexPath]
        collectionView.insertItems(at: indexPaths)
        pageControl.numberOfPages = pageControl.numberOfPages + 1
        dismiss(animated: true, completion: nil)
    }
    
    func addOrEditCategoryItemVC(_ controller: AddOrEditCategoryItemVC, didFinishEditing cateogryItem: CategoryItems) {
        if let index = category.items.firstIndex(where: {$0 === cateogryItem}) {
            let indexPath = IndexPath(item: index, section: 0)
            let cell = collectionView.cellForItem(at: indexPath) as! AdminCategoryItemCell
            cell.itemNameTextField.text = cateogryItem.itemName
            cell.itemDescriptionTextView.text = cateogryItem.itemDescription
            cell.itemPriceTextField.text = String(cateogryItem.itemPrice)
            cell.itemImageView.image = UIImage(named: cateogryItem.imageName)
        }
        dismiss(animated: true, completion: nil)
    }
}

extension AdminCategoryItemsVC: AdminCategoryItemCellDelegate {
    func didEdit(indexPath: IndexPath) {
        let navigationController = storyboard!.instantiateViewController(identifier: Identifier.navigationControllerIdentifier) as! UINavigationController
        let controller = navigationController.topViewController as! AddOrEditCategoryItemVC
        controller.delegate = self
        controller.itemToEdit = category.items[indexPath.item]
        present(navigationController, animated: true)
    }
    
    func didDelete(indexPath: IndexPath) {
        category.items.remove(at: indexPath.item)
        let indexpaths = [indexPath]
        pageControl.numberOfPages = pageControl.numberOfPages-1
        collectionView.deleteItems(at: indexpaths)
    }
}



  
 
   
  

  
