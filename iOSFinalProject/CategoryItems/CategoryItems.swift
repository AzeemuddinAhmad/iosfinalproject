import Foundation

class CategoryItems: NSObject, NSCoding {
    var itemName = ""
    var itemDescription = ""
    var imageName = ""
    var itemPrice: Float = 0
    override init() {
        super.init()
    }
    
    init(itemName: String, itemDescription: String, imageName: String, itemPrice: Float) {
        self.itemName = itemName
        self.itemDescription = itemDescription
        self.imageName = imageName
        self.itemPrice = itemPrice
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        itemName = aDecoder.decodeObject(forKey: "ItemName") as! String
        itemDescription = aDecoder.decodeObject(forKey: "ItemDescription") as! String
        imageName = aDecoder.decodeObject(forKey: "ImageName") as! String
        itemPrice = aDecoder.decodeFloat(forKey: "ItemPrice")
        
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(itemName, forKey: "ItemName")
        aCoder.encode(itemDescription, forKey: "ItemDescription")
        aCoder.encode(imageName, forKey: "ImageName")
        aCoder.encode(itemPrice, forKey: "ItemPrice")
    }
}
