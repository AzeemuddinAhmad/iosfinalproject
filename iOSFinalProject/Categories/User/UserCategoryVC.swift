import UIKit

class UserCategoryVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!

    var categories: [Category]
    
    enum Identifier {
        static let reuseIdentifier = "UserCategoryCell"
        static let cellNameIdentifier = "UserCategoryCell"
        static let backgroundImageIdentifier = "CategoryBackground"
        static let plistIdentifier = "Categories.plist"
        static let userCategoryItemSegue = "UserCategoryItems"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundView = UIImageView(image: UIImage(named: Identifier.backgroundImageIdentifier))
        let cellNib = UINib(nibName: Identifier.cellNameIdentifier, bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: Identifier.reuseIdentifier)
        print("\(dataFilePath())")
    }
    
    required init?(coder aDecoder: NSCoder) {
        categories = [Category]()
        super.init(coder: aDecoder)
        loadCategories()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Identifier.userCategoryItemSegue {
            let controller = segue.destination as! UserCategoryItemVC
            controller.category = sender as! Category
        }
    }
    
    func configureCell(cell: UserCategoryCell, index: Int) {
        let category = categories[index]
        cell.layer.cornerRadius = 20
        cell.label.text = category.name
        cell.imageView.image = UIImage(named: category.name)
    }
}

extension UserCategoryVC {
    fileprivate func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    fileprivate func dataFilePath() -> URL {
        return documentsDirectory().appendingPathComponent(Identifier.plistIdentifier)
    }
    
    fileprivate func loadCategories() {
        let path = dataFilePath()
        if let data = try? Data(contentsOf: path) {
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            categories = unarchiver.decodeObject(forKey: "Categories") as! [Category]
        }
    }
}

extension UserCategoryVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifier.reuseIdentifier, for: indexPath) as! UserCategoryCell
        configureCell(cell: cell, index: indexPath.item)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let category = categories[indexPath.item]
        performSegue(withIdentifier: Identifier.userCategoryItemSegue, sender: category)
    }
    
}

extension UserCategoryVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 10
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
}

extension UserCategoryVC {
    @IBAction func signOut() {
        let alertController = UIAlertController(title: "Sign Out?", message: "Do you want to sign out?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Sign Out", style: .default, handler: {action in self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)})
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(action)
        alertController.addAction(cancel)
        present(alertController, animated: true)
    }
}
