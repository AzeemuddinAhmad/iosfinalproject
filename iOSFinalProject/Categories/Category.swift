import Foundation

class Category: NSObject, NSCoding {
    var name = ""
    var imageName: String
    var items = [CategoryItems]()
    
    convenience init(name: String) {
        self.init(name: name, imageName: "No Icon")
    }
    
    init(name: String, imageName: String) {
        self.name = name
        self.imageName = imageName
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        name = aDecoder.decodeObject(forKey: "Name") as! String
        items = aDecoder.decodeObject(forKey: "CategoryItems") as! [CategoryItems]
        imageName = aDecoder.decodeObject(forKey: "ImageName") as! String
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "Name")
        aCoder.encode(items, forKey: "CategoryItems")
        aCoder.encode(imageName, forKey: "ImageName")
    }
}
