import UIKit

protocol AddOrEditCategoryTVCDelegate: class {
    func addOrEditCategoryTVCDidCancel(_ controller: AddOrEditCategoryTVC)
    func addOrEditCategoryTVC(_ controller: AddOrEditCategoryTVC, didFinishAdding category: Category)
    func addOrEditCategoryTVC(_ controller: AddOrEditCategoryTVC, didFinishEditing category: Category)
}

class AddOrEditCategoryTVC: UITableViewController {
    
    @IBOutlet weak var categoryNameTextField: UITextField!
    @IBOutlet weak var categoryImageView: UIImageView!

    var categoryToEdit: Category?
    weak var delegate: AddOrEditCategoryTVCDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorColor = UIColor.clear
        if let category = categoryToEdit {
            title = "Edit Category"
            categoryNameTextField.text = category.name
        }
    }

}

extension AddOrEditCategoryTVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBAction private func cancel(_ sender: Any) {
        delegate?.addOrEditCategoryTVCDidCancel(self)
    }
    
    @IBAction private func done(_ sender: Any) {
        if let category = categoryToEdit {
            category.name = categoryNameTextField.text!
            delegate?.addOrEditCategoryTVC(self, didFinishEditing: category)
        } else {
            let category = Category.init(name: categoryNameTextField.text!, imageName: "Background")
            delegate?.addOrEditCategoryTVC(self, didFinishAdding: category)
        }
    }
    
    @IBAction fileprivate func selectPicture(_ sender: Any) {
        let imagePicker = UIImagePickerController()
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard  let image = info[.originalImage] as? UIImage else {
            return
        }
        categoryImageView.image = image
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

extension AddOrEditCategoryTVC {
    override func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if indexPath.section == 1 {
            return indexPath
        } else {
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let section1Height: CGFloat = 106
        let section1RowHeight: CGFloat = 50
        if indexPath.section == 1 {
            return tableView.safeAreaLayoutGuide.layoutFrame.height - section1Height
        } else {
            return section1RowHeight
        }
    }
}
