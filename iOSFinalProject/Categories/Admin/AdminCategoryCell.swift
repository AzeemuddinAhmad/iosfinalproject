import UIKit

protocol AdminCategoryCellDelegate: class {
    func didEdit(indexPath: IndexPath)
    func didDelete(indexPath: IndexPath)
}

class AdminCategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    
    weak var delegate: AdminCategoryCellDelegate?
    var indexPath: IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
    }
}

extension AdminCategoryCell {
    @IBAction func didEdit(_ sender: Any) {
        delegate?.didEdit(indexPath: indexPath!)
    }
    
    @IBAction func didDelete(_ sender: Any) {
        delegate?.didDelete(indexPath: indexPath!)
    }
}
