import UIKit

class AdminCategoryVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!

    var categories: [Category]
    
    enum Identifier {
        static let reuseIdentifier = "CategoryCell"
        static let cellNameIdentifier = "AdminCategoryCell"
        static let backgroundImageIdentifier = "CategoryBackground"
        static let plistIdentifier = "Categories.plist"
        static let adminCategoryItemsSegue = "AdminCategoryItems"
    }
    
    enum ViewControllerSegue: String {
        case adminCategoryItems = "AdminCategoryItems"
        case addCategory = "AddCategory"
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundView = UIImageView(image: UIImage(named: Identifier.backgroundImageIdentifier))
        let cellNib = UINib.init(nibName: Identifier.cellNameIdentifier, bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: Identifier.reuseIdentifier)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        categories = [Category]()
        super.init(coder: aDecoder)
        print("\(dataFilePath())")
        loadCategories()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let identifier = segue.identifier,
        let identifierCase = AdminCategoryVC.ViewControllerSegue(rawValue: identifier) else {
            assertionFailure("Segue had no identifier")
            return
        }
        switch identifierCase {
        case .adminCategoryItems:
            let controller = segue.destination as! AdminCategoryItemsVC
            controller.category = (sender as! Category)
            
        case .addCategory:
            let navigationController = segue.destination as! UINavigationController
            let controller = navigationController.topViewController as! AddOrEditCategoryTVC
            controller.delegate = self
        }
    }
}

extension AdminCategoryVC {
    fileprivate func documentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    fileprivate func dataFilePath() -> URL {
        return documentsDirectory().appendingPathComponent(Identifier.plistIdentifier)
    }
    
    fileprivate func saveCategories() {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        archiver.encode(categories, forKey: "Categories")
        archiver.finishEncoding()
        data.write(to: dataFilePath(), atomically: true)
    }
    
    fileprivate func loadCategories() {
        let path = dataFilePath()
        if let data = try? Data(contentsOf: path) {
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            categories = unarchiver.decodeObject(forKey: "Categories") as! [Category]
        }
    }
    
    func configureCell(cell: AdminCategoryCell, index: Int) {
        let category = categories[index]
        cell.delegate = self
        cell.indexPath = IndexPath(item: index, section: 0)
        cell.contentView.layer.cornerRadius = 20
        cell.categoryNameLabel.text = category.name
        cell.categoryImageView.image = UIImage(named: category.name)
    }
}

extension AdminCategoryVC {
    @IBAction func signOut(_ sender: Any) {
        saveCategories()
        let alertController = UIAlertController(title: "Sign Out?", message: "Do you want to sign out?", preferredStyle: .alert)
        let action = UIAlertAction(title: "Sign Out", style: .default, handler: {action in self.dismiss(animated: true, completion: nil)})
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(action)
        alertController.addAction(cancel)
        present(alertController, animated: true)
    }
}

extension AdminCategoryVC: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Identifier.reuseIdentifier, for: indexPath) as! AdminCategoryCell
        
        configureCell(cell: cell,index: indexPath.item)
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let category = categories[indexPath.item]
        performSegue(withIdentifier: Identifier.adminCategoryItemsSegue, sender: category)
    }
}

extension AdminCategoryVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let padding: CGFloat = 10
        let collectionViewSize = collectionView.frame.size.width - padding
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
}

extension AdminCategoryVC: AddOrEditCategoryTVCDelegate {
    func addOrEditCategoryTVCDidCancel(_ controller: AddOrEditCategoryTVC) {
        dismiss(animated: true, completion: nil)
    }
    
    func addOrEditCategoryTVC(_ controller: AddOrEditCategoryTVC, didFinishAdding category: Category) {
        let newItemIndex = categories.count
        categories.append(category)
        
        let indexPath = IndexPath(item: newItemIndex , section: 0)
        let indexPaths = [indexPath]
        collectionView.insertItems(at: indexPaths)
        dismiss(animated: true, completion: nil)
        saveCategories()
    }
    
    func addOrEditCategoryTVC(_ controller: AddOrEditCategoryTVC, didFinishEditing category: Category) {
        if let index = categories.firstIndex(where: {$0 === category}) {
            let indexPath = IndexPath(item: index, section: 0)
            let cell = collectionView.cellForItem(at: indexPath) as! AdminCategoryCell
            cell.categoryNameLabel.text = category.name
        }
        dismiss(animated: true, completion: nil)
        saveCategories()
    }
    
}

extension AdminCategoryVC: AdminCategoryCellDelegate {
    func didEdit(indexPath: IndexPath) {
        let navigationController = storyboard?.instantiateViewController(withIdentifier: "AddOrEditCategoryNC") as! UINavigationController
        let controller = navigationController.topViewController as! AddOrEditCategoryTVC
        controller.delegate = self
        controller.categoryToEdit = categories[indexPath.item]
        present(navigationController, animated: true, completion: nil)
    }
    
    func didDelete(indexPath: IndexPath) {
        categories.remove(at: indexPath.item)
        let indexPaths = [indexPath]
        collectionView.deleteItems(at: indexPaths)
    }
    
    
}
